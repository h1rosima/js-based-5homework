
function Division () {
    let numFirst = parseInt(prompt("Введите делимое: "));
    let numSecond = parseInt(prompt("Введите делитель: "));

    let result = numFirst / numSecond;

    return `Вы поделили делимое: ${numFirst} на делитель: ${numSecond} и получили частное: ${result}.`;
}

console.log(Division());


//2


function mathCalc () {
    let action = prompt('Введите действие которое вы хотите совершить. Доступно только (+,-,*,/).');
    let numFirst = parseInt(prompt("Введите первое целое число: "));
    let numSecond = parseInt(prompt("Введите второе целое число: "));
    let result;

    if (isNaN(numFirst) || isNaN(numSecond)) {
        alert(`Ошибка. Введите число корректно.`)
    }

    switch (action) {
        case '+':
            result = numFirst + numSecond;
            break;

        case '-':
            result = numFirst - numSecond;
            break;

        case '*':
            result = numFirst * numSecond;
            break;

        case '/':
            if (numSecond === 0) {
                alert('Деление на 0 невозможно.');
                return;
            } else {
                result = numFirst / numSecond;
            }
            break;

        default:
            alert('Невозможная операция.')
    }
    return `Результат: ${result}`;
}
console.log(mathCalc());
